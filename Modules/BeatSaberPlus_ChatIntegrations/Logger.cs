﻿namespace BeatSaberPlus_ChatIntegrations
{
    /// <summary>
    /// Logger instance holder
    /// </summary>
    internal class Logger
    {
        /// <summary>
        /// Logger instance
        /// </summary>
        internal static IPA.Logging.Logger Instance;
    }
}
