


# BeatSaberPlus (BS+)

**Discord** https://discord.gg/63ebPMC (**Download, Support, Testing, Early releases** and cool new mods coming soon)
**Patreon** [https://www.patreon.com/BeatSaberPlus](https://www.patreon.com/BeatSaberPlus) **if you want to support this project development!**

Current version : 4.4.0

IMPORTANT Most of the modules are disabled by default, you can enable then in BeatSaberPlus -> Settings
IMPORTANT When you enable Chat module, it will open a page in your browser for configuring it.

## Main features:

 - **Chat:** ([Documentation](https://github.com/hardcpp/BeatSaberPlus/wiki#chat))
	 - Display in game your Twitch & Youtube (Patreons only) chat (7TV & FFZ & BTTV support).
	 - See subscriptions, follow, raid, bits events, channel redeems (channel points).
	 - Twitch: Polls, Predictions(bets), HypeTrains
	 - Viewer count.
	 - Lot of customization options (Color, Size, Filter TTS messages, hide specific events...).
 - **ChatEmoteRain:** ([Documentation](https://github.com/hardcpp/BeatSaberPlus/wiki#chat-emote-rain))
	 - See emotes used in chat raining in game!
	 - Support **GIF / APNG / PNG / WEBP** files.
	 - Custom emote rain when someone subscribe to your Twitch channel.
	 - Advanced configuration options.
 - **ChatRequest:** ([Documentation](https://github.com/hardcpp/BeatSaberPlus/wiki#chat-request))
	 - Chat request system that allow your viewers to make requests with [https://beatsaver.com/](https://beatsaver.com/) website.
	 - Display information about all difficulties for a song including NPS/Offset.
	 - Display song description, votes, upload date when you select a song.
	 - Display scores on song when you over one.
	 - User, Mapper, Song ban system
	 - Safe mode that hide any sensitive informations (Song name, artist, uploader..)
	 - History & blacklist tab that let you see your request history and manage your blacklist.
	 **- An intelligent "!link" command that show current played song or last one and provide a link to beatsaver.com if the map is public, the command also works outside of request and in multiplayer!**
 - **GameTweaker:**  ([Documentation](https://github.com/hardcpp/BeatSaberPlus/wiki#game-tweaker))
	 - **Can remove note debris, cut particles, obstacles particles, floor burn particles, floor burn effects, saber clash particles, world particles.**
	 - **Precise reaction time (AKA offset) selection**
	 - **Add an override light intensity option that let you boost/dim lights from 0% to 2000% (also work in static lights).**
	 - Can remove BTS/LinkinPark assets in play environment, FullCombo loss animation, Editor button on the main menu, Promotional content from the menu.
	 - Can re-order player options menu for better accessibility.
 - **MenuMusic:** ([Documentation](https://github.com/hardcpp/BeatSaberPlus/wiki#menu-music))
	 - Replace menu ambient sounds with music!
	 - Play any songs from your custom levels or you own selection of music!
	 - Player interface shows in the menu on left with Prev/Random/Play/Pause/Next buttons and with a volume selector.
	 - **A play button to play the current song level**.
 - **SongChartVisualizer:** ([Documentation](https://github.com/hardcpp/BeatSaberPlus/wiki#song-chart-visualizer))
	 - Preview map difficulty with a nice and beautiful graph in game that illustrate NPS (Notes per second).
	 - Support for 90 & 360 levels.
	 - Lot of customization options (Colors, Legend, Dash lines...).

## Dependencies:

- SongCore [https://github.com/Kylemc1413/SongCore](https://github.com/Kylemc1413/SongCore)
- BeatSaberMarkupLanguage [https://github.com/monkeymanboy/BeatSaberMarkupLanguage](https://github.com/monkeymanboy/BeatSaberMarkupLanguage)

## **Special Thanks**:
- **Vred#0001** For art & documentation
- **Brase#6969** For documentation
- **Crafang#8040** For documentation & translation
- **Lucy#9197** For documentation
- **redegg89#9290** For Documentation syntax/grammar

## **Discord & Download/Update**
https://discord.gg/63ebPMC 

## **Credits / Copyright**
* [EnhancedStreamChat-v3](https://github.com/brian91292/EnhancedStreamChat-v3)
* [TournamentAssistant](https://github.com/MatrikMoon/TournamentAssistant)
* [Beat-Saber-Utils](https://github.com/Kylemc1413/Beat-Saber-Utils)
* [BeatSaverDownloader](https://github.com/Kylemc1413/BeatSaverDownloader)
* [BeatSaberMarkupLanguage](https://github.com/monkeymanboy/BeatSaberMarkupLanguage)
* [websocket-sharp](https://github.com/sta/websocket-sharp)

## **Screenshots**
![](https://puu.sh/GO6tf/81ff167aab.png)
![](https://puu.sh/GKKJJ/7a481941c5.png)
![](https://puu.sh/GKPcD/ecee2e5d86.png)
![](https://puu.sh/GH9Rn/d9d4966a04.png)
![](https://puu.sh/GH9RA/f6dc522cd1.png)
![](https://puu.sh/GL7BX/0e5f12cfce.jpg)

