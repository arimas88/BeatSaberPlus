﻿namespace BeatSaberPlus.SDK.Chat.Interfaces
{
    public interface IChatChannel
    {
        string Name { get; }
        string Id { get; }
    }
}
