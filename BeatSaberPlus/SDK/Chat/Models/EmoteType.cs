﻿namespace BeatSaberPlus.SDK.Chat.Models
{
    public enum EmoteType
    {
        SingleImage = 0,
        SpriteSheet = 1
    }
}
