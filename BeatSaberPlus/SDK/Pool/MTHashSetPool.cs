using System.Collections.Generic;

namespace BeatSaberPlus.Pool
{
    /// <summary>
    /// A version of Pool.CollectionPool_2 for HashSets.
    /// </summary>
    public class MTHashSetPool<T> : MTCollectionPool<HashSet<T>, T>
    {

    }
}