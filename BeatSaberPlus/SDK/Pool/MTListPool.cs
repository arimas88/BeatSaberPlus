using System.Collections.Generic;

namespace BeatSaberPlus.Pool
{
    /// <summary>
    /// A version of Pool.CollectionPool_2 for Lists.
    /// </summary>
    public class MTListPool<T> : MTCollectionPool<List<T>, T>
    {

    }
}